FROM python:3.6

WORKDIR /app

ADD requirements.txt /app

ADD producer.py /app

RUN pip3 install -r requirements.txt

#RUN docker run -d -p 15672:15672 -p 5672:5672 rabbitmq:3-management

CMD ["python","./producer.py"]
