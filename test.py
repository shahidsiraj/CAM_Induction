import requests
import pandas as pd
import adal

tenantID = "eff7f985-dc58-4935-a906-050609be85c3"
grant_type = "client_credentials"
client_id = "f2642234-880a-465f-bf1a-7fc31130f2fd"
client_secret = "Ld++kH96y5SHXmiGUkdTex44MvOokxrHfXsRemfbL8c="
subscription_id = "64caccf3-b508-41e7-92ed-d7ed95b32621"
resource = "https://management.azure.com/"

df = pd.DataFrame()


# url = "https://login.microsoftonline.com/"+tenantID+"/oauth2/token"
# payload = {"grant_type":grant_type,
#            "client_id":client_id,
#            "client_secret":client_secret,
#            "resource":resource
#            }
# headers = {
#     'Content-Type': "application/x-www-form-urlencoded",
#     }
#
# r = requests.post(url=url, data=payload,headers=headers)
auth_endpoint = "https://login.microsoftonline.com/"+tenantID
context = adal.AuthenticationContext(auth_endpoint,api_version=None)
r= context.acquire_token_with_client_credentials(resource, client_id, client_secret)
token = r['accessToken']


vm_url = "https://management.azure.com/subscriptions/"+subscription_id+"/providers/Microsoft.Compute/virtualMachines?api-version=2017-12-01"
vm_header = {
    "Authorization": "Bearer "+token,
}
vm_r = requests.get(url=vm_url,headers=vm_header)



for i in vm_r.json()['value']:
    id = i['id']
    metric_url = "https://management.azure.com"+id+"/providers/microsoft.insights/metrics?api-version=2017-05-01-preview&timespan=2018-06-29/2018-06-30&interval=PT1H&metric=Percentage CPU"
    metric_r = requests.get(url=metric_url,headers=vm_header)
    raw_data = metric_r.json()
    for j in raw_data['value'][0]['timeseries'][0]['data']:
        print(j)
        raw = {
            "vm_id": id,
            "timeStamp": j['timeStamp'],
        }
        if 'average' in j.keys():
            raw['average']=j['average']
        else:
            raw['average']='0'
        df = df.append(pd.DataFrame([raw],columns=['vm_id','timeStamp','average']))

df.to_csv('percentageCPU.csv',index=False)