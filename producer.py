from kombu import Connection,Queue,Exchange,Producer

print("1. List all vms\n"+"2. List all storage")
x= input("Enter input:")
rabbit_url = "amqp://172.27.2.167:5672/"
conn = Connection(rabbit_url)
channel = conn.channel()
exchange = Exchange("example-exchange", type="direct")
producer = Producer(exchange=exchange, channel=channel, routing_key="BOB")
queue = Queue(name="example-queue", exchange=exchange, routing_key="BOB")
queue.maybe_bind(conn)
queue.declare()
producer.publish(x)